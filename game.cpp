#include <string>
#include <iostream>
#include <fstream>
#include "SDL/SDL.h"
#include "game.h"
#include "timer.h"
#include "paddle.h"
#include "ball.h"

SDL_Surface *screen;
SDL_Surface *paddle;
SDL_Surface *ball;
SDL_Surface *blocks;

Paddle* paddleObj = NULL;
Ball* ballObj = NULL;

int map[MAPWIDTH][MAPHEIGHT];

bool move_left = false;
bool move_right = false;

bool quit = false;
bool reset = false;

SDL_Surface *load_image( std::string filename ) {

    //Temporary storage for the image that's loaded
    SDL_Surface* loadedImage = NULL;
    
    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;

    //Load the image
    loadedImage = SDL_LoadBMP( filename.c_str() );

    //If nothing went wrong in loading the image
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );
        
        //Free the old image
        SDL_FreeSurface( loadedImage );
    }

    //Return the optimized image
    return optimizedImage;
}

/*void DrawTile(int x,int y,int tile) {
    //Draw the Tile
    Blit(screen, x*TILESIZE, y*TILESIZE, blocks, tile*TILESIZE, 0, TILESIZE, TILESIZE);
}*/

void Blit(SDL_Surface *Dest, int DestX, int DestY, SDL_Surface *Src) {
    //Create a Rect, and store the coordinates in it.
    //Because SDL likes it this way. 
    SDL_Rect DestR;
    DestR.x = DestX; DestR.y = DestY;

    //Draw to the Dest
    SDL_BlitSurface(Src, NULL, Dest, &DestR);
}

//-----------------------------------------------------------------------------
void Blit(SDL_Surface *Dest, int DestX, int DestY, SDL_Surface *Src, int SrcX, int SrcY, int SrcW, int SrcH) {
    //Create a 2 Rects. The first is for where we want to Blit to.
    //The other is for clipping the 'Src' so we only draw the portion we want

    SDL_Rect DestR;  SDL_Rect SrcR;
    DestR.x = DestX; DestR.y = DestY;
    SrcR.x = SrcX;   SrcR.y = SrcY;
    SrcR.w = SrcW;   SrcR.h = SrcH;

    //Draw to the Dest
    SDL_BlitSurface(Src, &SrcR, Dest, &DestR);
}


void DrawBrick(int x,int y,int tile) {
    //Draw the Tile
    Blit(screen, x*BRICKWIDTH, y*BRICKHEIGHT, blocks, tile*BRICKWIDTH, 0, BRICKWIDTH, BRICKHEIGHT);
}

void DrawMap(){

    int i, j;

    SDL_FillRect( screen, &screen->clip_rect, SDL_MapRGB( screen->format, 0x00, 0x00, 0x00 ) );

    for(int i=0; i<MAPWIDTH; i++)
        for(int j=0; j<MAPHEIGHT; j++){
            if(map[i][j] != NOBRICK)
                DrawBrick(i,j,map[i][j]);
        }

    Blit(screen, paddleObj->getPos(), SCREENHEIGHT - PLAYERYPOSITION, paddle);
    Blit(screen, ballObj->getPosX(), ballObj->getPosY(), ball);
    
    SDL_Flip(screen);

}

int getPosXOnMap(int x){
    return ((int)x/MAPWIDTH);
}

int getPosYOnMap(int y){
    if (y > MAPHEIGHT*BRICKHEIGHT)
        return -1;
    else
        return ((int)y/BRICKHEIGHT) - 1;
}

void OnEvent(){

    SDL_Event event;
    
    if( SDL_PollEvent( &event ) )
            {
                //If a key was pressed
                if( event.type == SDL_KEYDOWN )
                {
                    //Set the proper message surface
                    switch( event.key.keysym.sym )
                    {
                        case SDLK_LEFT: move_left = true; break;
                        case SDLK_RIGHT: move_right = true; break;
                    }
                }

                else if (event.type == SDL_KEYUP){

                    switch (event.key.keysym.sym){
                        case SDLK_LEFT: move_left = false; break;
                        case SDLK_RIGHT: move_right = false; break;
                    }
                }
                
                //If the user has Xed out the window
                else if( event.type == SDL_QUIT )
                {
                    //Quit the program
                    reset = true;
                    quit = true;
                }
            }
}

int GameInit(){

    //Initialize all SDL subsystems
    if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        return 1;    
    }



    //Set up the screen
    screen = SDL_SetVideoMode( SCREENWIDTH, SCREENHEIGHT, SCREENBPP, SDL_SWSURFACE );


    //If there was an error in setting up the screen
    if( screen == NULL )
    {
        return 1;    
    }

    //Set the window caption
    SDL_WM_SetCaption( "BREAKOUT!", NULL );

    //Load the images
    paddle = load_image( "paddle.bmp" );
    ball = load_image( "ball.bmp" );
    blocks = load_image( "bricks.bmp" );

    paddleObj = new Paddle((int)SCREENWIDTH/2, (int)SCREENHEIGHT - PLAYERYPOSITION, paddle->h, paddle->w, paddle);
    ballObj = new Ball((int)SCREENWIDTH/2, (int)SCREENHEIGHT - PLAYERYPOSITION*2, (int)ball->h/2, ball, BALLSPEED, BALLSPEED);

    return 0;


}

bool collision( Ball* ball, Paddle* paddle ){

    if(ball->getBottomSidePositionY() == SCREENHEIGHT-PLAYERYPOSITION){
        if(paddle->getPos() < ball->getCentrePosX()+ball->getRadius() && paddle->getPos() > ball->getCentrePosX()-ball->getRadius()-paddle->getWidth())
            return true;
    }
    else
        return false;
}

void TopCollision(){
    if(getPosYOnMap(ballObj->getTopSidePositionY()) != -1){
        if(map[getPosXOnMap(ballObj->getTopSidePositionX())][getPosYOnMap(ballObj->getTopSidePositionY())] != NOBRICK){
            map[getPosXOnMap(ballObj->getTopSidePositionX())][getPosYOnMap(ballObj->getTopSidePositionY())] = NOBRICK;
            ballObj->setVel(ballObj->getVelX(), -ballObj->getVelY());
        }
    }
}

void BottomCollision(){
    if(getPosYOnMap(ballObj->getBottomSidePositionY()) != -1){
        if(map[getPosXOnMap(ballObj->getBottomSidePositionX())][getPosYOnMap(ballObj->getBottomSidePositionY())] != NOBRICK){
            map[getPosXOnMap(ballObj->getBottomSidePositionX())][getPosYOnMap(ballObj->getBottomSidePositionY())] = NOBRICK;
            ballObj->setVel(ballObj->getVelX(), -ballObj->getVelY());
        }
    }
}

void LeftCollision(){
    if(getPosYOnMap(ballObj->getCentrePosY()) != -1){
        if(map[getPosXOnMap(ballObj->getLeftSidePositionX())][getPosYOnMap(ballObj->getLeftSidePositionY())] != NOBRICK){
            map[getPosXOnMap(ballObj->getLeftSidePositionX())][getPosYOnMap(ballObj->getLeftSidePositionY())] = NOBRICK;
            ballObj->setVel(-ballObj->getVelX(), ballObj->getVelY());
        }
    }
}

void RightCollision(){
    if(getPosYOnMap(ballObj->getCentrePosY()) != -1){
        if(map[getPosXOnMap(ballObj->getRightSidePositionX())][getPosYOnMap(ballObj->getRightSidePositionY())] != NOBRICK){
            map[getPosXOnMap(ballObj->getRightSidePositionX())][getPosYOnMap(ballObj->getRightSidePositionY())] = NOBRICK;
            ballObj->setVel(-ballObj->getVelX(), ballObj->getVelY());
        }
    }
}

void CheckCollisions(){

    if(collision(ballObj, paddleObj)){
        ballObj->setVel(ballObj->getVelX(), -ballObj->getVelY());
    }

    /*if(getPosYOnMap(ballObj->getPosY()) != -1){
        if(map[getPosXOnMap(ballObj->getPosX())][getPosYOnMap(ballObj->getPosY()) - 1] != NOBRICK){
            map[getPosXOnMap(ballObj->getPosX())][getPosYOnMap(ballObj->getPosY()) - 1] = NOBRICK;
            ballObj->setVel(ballObj->getVelX(), -ballObj->getVelY());
        }
    }*/

    TopCollision();
    LeftCollision();
    RightCollision();
    BottomCollision();

}

void MoveObjects(){

    if(move_left)
        paddleObj->move(LEFT);
    if(move_right)   
        paddleObj->move(RIGHT);

    ballObj->move();
}

void LoadLevel(std::string file){

    int colour;

    std::ifstream levelFile(file.c_str());
    if(levelFile.is_open()){
        while(levelFile.good()){
            for(int j = 0; j < MAPHEIGHT; j++){
                for(int i = 0; i<MAPWIDTH; i++){
                    map[i][j] = levelFile.get() - 48;
                }
                colour = levelFile.get();
            }
        }
    }
}

int main( int argc, char* args[] )
{

    if(GameInit()){
        return 1;
    }

    SDL_SetColorKey( ball, SDL_SRCCOLORKEY, SDL_MapRGB(ball->format, 255, 0, 255) ); //set pink as transparent colour

    Timer fps; //regulating fps

    LoadLevel("level1.lvl");

    for(int i = 0; i<MAPHEIGHT; i++){
        for(int j = 0; j<MAPWIDTH; j++){
            std::cout << map[j][i];
        }
        std::cout << std::endl;
    }


    while(!quit){

        //initial positions for ball and paddle
        Blit(screen, (int)SCREENWIDTH/2, (int)SCREENHEIGHT - PLAYERYPOSITION, paddle);
        Blit(screen, (int)SCREENWIDTH/2, (int)SCREENHEIGHT - PLAYERYPOSITION*2, ball);
        
        //game loop
        reset = false;

        ballObj->setPos((int)SCREENWIDTH/2, (int)SCREENHEIGHT - PLAYERYPOSITION*4);
        paddleObj->setPos(SCREENWIDTH/2-(paddleObj->getWidth())/2);

        SDL_Delay(500);

        while (!reset){

            fps.start(); // start the timer for fps control

            OnEvent(); //keyboard input for paddle and quitting    

            //if(collision(ballObj, paddleObj))
            //    ballObj->setVel(-ballObj->getVelX(), ballObj->getVelY());  
            
            
            CheckCollisions();
            
            if(ballObj->getBottomSidePositionY() == SCREENHEIGHT - ballObj->getRadius())
                reset = true;

            MoveObjects(); //applies physics to ball and paddle

            DrawMap(); //screen flip happens here

            std::cout << getPosXOnMap(ballObj->getCentrePosX()) << ", " << getPosYOnMap(ballObj->getCentrePosY()) << std::endl;
            //std::cout << ballObj->getPosX() << ", " << ballObj->getPosY() << std::endl;
            //Cap the frame rate
            if( fps.get_ticks() < 1000 / FRAMES_PER_SECOND )
            {
                SDL_Delay( ( 1000 / FRAMES_PER_SECOND ) - fps.get_ticks() );
            }

            
        }
    }
    

    //Free the surfaces
    SDL_FreeSurface( paddle );
    SDL_FreeSurface( ball );
    
    //Quit SDL
    SDL_Quit();
    
    //Return
    return 0;
}
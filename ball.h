#include "SDL/SDL.h"

class Ball {

	public:
		Ball();
		Ball(int x, int y, int rad, SDL_Surface* image, int velx, int vely);
		~Ball(){};

		int getPosX();
		int getPosY();
		void setPos(int x, int y);

		int getCentrePosX();
		int getCentrePosY();
		
		int getHeight();
		int getWidth();
		void setSize(int height, int width); //use with image size

		//int getPosXOnMap(); //returns the x position of the ball on the game map array
		//int getPosYOnMap();

		int getRadius();
		int setRadius(int rad);

		int getLeftSidePositionX();
		int getLeftSidePositionY();

		int getRightSidePositionX();
		int getRightSidePositionY();

		int getTopSidePositionX();
		int getTopSidePositionY();

		int getBottomSidePositionX();
		int getBottomSidePositionY();
		
		int getVelX();
		int getVelY();
		void setVel(int x, int y);

		void move();
		void setImage(SDL_Surface* image);

		SDL_Rect getBox();
	private:
		int posx, posy;
		int width, height, r;
		int velx, vely;
		SDL_Surface* image;
		SDL_Rect box;
};
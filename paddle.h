#include "SDL/SDL.h"

class Paddle {

	public:
		Paddle();
		Paddle(int x, int y, int objHeight, int objWidth, SDL_Surface* objImage);
		~Paddle(){};

		int getPos();
		void setPos(int x);
		int getHeight();
		int getWidth();
		void setSize(int heightarg, int widtharg); //use with image size
		void move(int dir); //1 is right, -1 is left
		void setImage(SDL_Surface* image);

		SDL_Rect getBox();
	private:
		int posx, posy;
		int height, width;
		SDL_Surface* image;
		SDL_Rect box;
};
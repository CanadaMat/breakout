#include "paddle.h"
#include "game.h"

Paddle::Paddle(){

}

Paddle::Paddle(int x, int y, int objHeight, int objWidth, SDL_Surface* objImage){
	posx = x;
	posy = y;
	height = objHeight;
	width = objWidth;
	this->image = objImage;
	box.x = posx;
	box.y = posy;
	box.w = width;
	box.h = height;
}

//Paddle::~Paddle(){};

int Paddle::getPos(){
	return posx;
}

void Paddle::setPos(int x) {
	posx = x;
}

int Paddle::getHeight(){
	return height;
}

int Paddle::getWidth(){
	return width;
}

void Paddle::setSize(int heightarg, int widtharg) {
	height = heightarg;
	width = widtharg;
}

void Paddle::move(int dir) {
	posx += dir*PADDLESPEED;
	if (posx <= PADDLELIMITLEFT)
		posx = PADDLELIMITLEFT;
	if (posx >= PADDLELIMITRIGHT - width)
		posx = PADDLELIMITRIGHT- width;
}

void Paddle::setImage(SDL_Surface* image) {
	this->image = image;
}

SDL_Rect Paddle::getBox(){ return box; }
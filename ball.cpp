#include "ball.h"
#include "game.h"

Ball::Ball(){}

Ball::Ball(int x, int y, int rad, SDL_Surface* image, int velxarg, int velyarg){
	posx = x;
	posy = y;
	velx = velxarg;
	vely = velyarg;
	r = rad;
	height = rad;
	width = rad;
	this->image = image;
	box.x = posx;
	box.y = posy;
	box.w = width;
	box.h = height;
}

//Ball::~Ball(){};

int Ball::getPosX(){ return posx; }

int Ball::getPosY(){ return posy; }

void Ball::setPos(int x, int y){
	posx = x;
	posy = y;
}

int Ball::getCentrePosX(){
	return posx + (int)width/2;
}

int Ball::getCentrePosY(){
	return posy + (int)height/2;
}

int Ball::getRadius(){
	return r;
}

int Ball::setRadius(int rad){
	r = rad;
}

int Ball::getHeight(){ return height; }

int Ball::getWidth(){ return width; }

void Ball::setSize(int heightarg, int widtharg){
	height = heightarg;
	width = widtharg;
}

/*int Ball::getPosXOnMap(){
	return (int)posx/MAPWIDTH;
}

int Ball::getPosYOnMap(){
	if (posy > MAPHEIGHT*BRICKHEIGHT)
		return -1;
	else
		return (int)posy/BRICKHEIGHT;
}*/

int Ball::getLeftSidePositionX(){
	return posx;
}

int Ball::getLeftSidePositionY(){
	return posy+r;
}

int Ball::getRightSidePositionX(){
	return posx+2*r;
}

int Ball::getRightSidePositionY(){
	return posy+r;
}

int Ball::getTopSidePositionX(){
	return posx+r;
}

int Ball::getTopSidePositionY(){
	return posy;
}

int Ball::getBottomSidePositionX(){
	return posx+r;
}

int Ball::getBottomSidePositionY(){
	return posy+2*r;
}

int Ball::getVelX(){ return velx; }

int Ball::getVelY(){ return vely; }

void Ball::setVel(int x, int y){
	velx = x;
	vely = y;
}

void Ball::move(){
	posx += velx;
	if(posx < 0 || posx > SCREENWIDTH - width*2)
		velx = -velx;

	posy -= vely;
	if(posy < 0 || posy > SCREENHEIGHT - height)
		vely = -vely;
}

void Ball::setImage(SDL_Surface* image){
	this->image = image;
}

SDL_Rect Ball::getBox(){ return box; }